package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class ResponseController {
    @RequestMapping("/api/no-return-value")
    public void voidRe() {
        return ;
    }

    @RequestMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void voidAnno() {
        return ;
    }

    @RequestMapping("/api/messages/{message}")
    public String getMessasge(@PathVariable("message") String message){
        return message;
    }




}
