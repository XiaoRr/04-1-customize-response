package com.twuc.webApp;

public class Message {

    //private final String value;
    private String value;

    public Message() {
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
