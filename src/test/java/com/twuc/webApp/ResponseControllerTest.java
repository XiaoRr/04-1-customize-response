package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc

public class ResponseControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    public void testNullRe() throws Exception {
        mockMvc.perform(get("/api/no-return-value")).andExpect(status().is(200));
    }

    @Test
    public void testReturnWithAnno() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation")).andExpect(status().is(204));
    }

    @Test
    public void testString() throws Exception {
        mockMvc.perform(get("/api/messages/message")).andExpect(content().string("message"))
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(status().is(200));

    }

    @Test
    public void testObj() throws Exception {
        mockMvc.perform(get("/api/messages/message")).andExpect(content().string("message"))
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(status().is(200));

    }
}
